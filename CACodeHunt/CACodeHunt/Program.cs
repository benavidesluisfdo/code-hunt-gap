﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.Data.Text;
using MathNet.Numerics.LinearAlgebra;
using System.Collections;
using Newtonsoft.Json;
using System.IO;

namespace CACodeHunt
{
    class Program
    {

        private static char[,] array2D;

        private static int Rowinit { set; get; }

        private static int Colinit { set; get; }

        private static int RowEnd { set; get; }

        private static int ColEnd { set; get; }

        private static bool Arrived = false;

        private static ArrayList Movements;
        private static ArrayList MovementsX;
        private static ArrayList MovementsY;
        static void Main(string[] args)
        {
            Colinit = 60-1;
            Rowinit = 83-1;
            ColEnd = 18-1;
            RowEnd = 26-1;            
            
            MovementsX = new ArrayList();
            MovementsY = new ArrayList();
            Movements = new ArrayList();

            // Read each line of the file into a string array. Each element
            // of the array is one line of the file.
            string[] lines = System.IO.File.ReadAllLines(@"C:\Temp\Hound Maze(tsv).txt");
            
            int[] GetMaxCol = new int[lines.Count()];
            int i = 0;

            foreach (string line in lines)
            {
                // Use a tab to indent each line of the file.                
                string line2 = line.Replace("\tF", "F");

                //Complete for offset of 5 newlines and 5 tabs.
                line2 = "\t" + line2;
                line2 = line2.Replace("\t", "0");  
                
                var Chars2 = line2.ToCharArray();
                GetMaxCol[i] = Chars2.Count();
                
                Console.WriteLine("\t" + line2);
                i++;
            }

            //Search bigger
            int SizeCol = Program.SearchMax(GetMaxCol);

            //Crear Matrix
            array2D = new char[lines.Count(), SizeCol];

            //Populate Matrix with data
            int k = 0;
            foreach (string line in lines)
            {
                // Use a tab to indent each line of the file.  
                string line2 = line.Replace("\tF", "F");

                //Complete for offset of 5 newlines and 5 tabs.
                line2 = "\t" + line2;

                line2 = line2.Replace("\t", "0");
                
                var Chars = line2.ToCharArray();               

                int j = 0;
                foreach (char item in Chars)
                {
                    array2D[k,j] = item;
                    j++;
                }                
                k++;
            }

            Rove(Rowinit, Colinit);
                        
            TakeBestWay(MovementsX, MovementsY);

            string sEmail = "benavides.luisfdo@gmail.com";
            string sGit = "https://bitbucket.org/benavidesluisfdo/code-hunt-gap";
            ArrayList sSolution = new ArrayList();      

            for (int l = 0; l <= MovementsX.Count-1; l++)
            {
                ArrayList ArSNew = new ArrayList();
                //Console.WriteLine("[" + MovementsY[l] + "," + MovementsX[l] + "]");
                ArSNew.Add(MovementsX[l]);
                ArSNew.Add(MovementsY[l]);
                sSolution.Add(ArSNew);                
            }

            CHSolution sol = new CHSolution { email = sEmail, repo = sGit, solution = sSolution };            
            string outputJSON = JsonConvert.SerializeObject(sol, Formatting.Indented);
            File.WriteAllText("CHSolution.json", outputJSON);

            // Keep the console window open in debug mode.
            Console.WriteLine("Press any key to exit.");
            System.Console.ReadKey();
        }

        public static int SearchMax(int[] Ar)
        {
            int Larger = Ar[Ar.Length - 1];

            for (int i = 0; i < Ar.Length-1; i++)
            {
                if (Ar[i] > Larger)
                    Larger = Ar[i];
            }

            return Larger;
        }

        private static bool FindMove(string move)
        {
            foreach (var item in Movements)
            {
                if (item.ToString().Equals(move))
                    return true;                                    
            }
            return false;
        }
        
        private static void TakeBestWay(ArrayList ArrX, ArrayList ArrY)
        {
            if (ArrX.Count > 2 && ArrY.Count > 2)
            {
                ArrayList indexs = new ArrayList();

                for (int i = 0; i < ArrX.Count - 1; i++)
                {
                    int currentx = Convert.ToInt32(ArrX[i]);
                    int nextx = Convert.ToInt32(ArrX[i + 1]);

                    int currenty = Convert.ToInt32(ArrY[i]);
                    int nexty = Convert.ToInt32(ArrY[i + 1]);

                    int differencex = Math.Abs(currentx - nextx);
                    int differencey = Math.Abs(currenty - nexty);

                    if ((differencex > 1) || (differencey > 1))
                    {                        
                        indexs.Add(i);
                    }
                    else
                        if (currentx != nextx && currenty != nexty)
                    {                     
                        indexs.Add(i);
                    }                  
                }

                if (indexs.Count > 0)
                {
                    //Eliminate dead ends
                    for (int j = indexs.Count - 1; j >= 0; j--)
                    {
                        int index = Convert.ToInt32(indexs[j]);
                        ArrX.RemoveAt(index);
                        ArrY.RemoveAt(index);
                    }

                    TakeBestWay(ArrX, ArrY);
                }
            }
        }
        
        private static void Rove(int fil, int col)
        {
            char test = Convert.ToChar("F");

            //Verify index in matrix
            int SizeCol = array2D.GetUpperBound(1);
            int SizeRow = array2D.GetUpperBound(0);
            string CurrentMove = "[" + fil + ", " + col + "]";

            if (fil <= SizeRow && col <= SizeCol & Arrived == false && !FindMove(CurrentMove))
            {

                if (array2D[fil, col].Equals(test) && fil == RowEnd && col == ColEnd)
                {
                    //End road                    
                    Movements.Add("[" + fil + ", " + col + "]");
                    MovementsX.Add(col);
                    MovementsY.Add(fil);
                    //Console.WriteLine("[" + fil + ", " + col + "]");
                    Arrived = true;
                }
                else
                    if (array2D[fil, col].Equals(test))
                {   
                    //Save the road                    
                    Movements.Add("[" + fil + ", " + col + "]");
                    MovementsX.Add(col);
                    MovementsY.Add(fil);
                    //Console.WriteLine("[" + fil + ", " + col + "]");

                    Rove(fil, col + 1);
                    if (col - 1 > -1)
                        Rove(fil, col - 1);
                    Rove(fil + 1, col);
                    if (fil - 1 > -1)
                        Rove(fil - 1, col);
                    
                }

            }
        }

    }
}
