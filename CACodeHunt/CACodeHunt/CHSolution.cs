﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CACodeHunt
{
    class CHSolution
    {
        public string email { get; set; }
        public string repo { get; set; }
        public ArrayList solution { get; set; }
        
    }
}
