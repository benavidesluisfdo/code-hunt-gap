# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository was created by the solution to a test arranged by GAP for Code Hunt
* Version 1.0

### How do I get set up? ###

* Configuration
The Hound Maze (tsv) .txt file should preferably be located in the path: C: \ Temp \ Hound Maze (tsv) .txt.
* Dependencies
You must obtain all the dependencies of the project in NutGet Package, to guarantee its correct operation.
* How to run
Run the application to get the solution file in CACodeHunt / CACodeHunt / bin / Debug / CHSolution.json.
* Deployment instructions
To open the entire project solution, open the file that is located in CACodeHunt / CACodeHunt.sln

